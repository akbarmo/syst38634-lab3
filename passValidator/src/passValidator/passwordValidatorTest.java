package passValidator;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class passwordValidatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCheckPasswordLength() {
		boolean password = passwordValidator.checkPasswordLength("Seena1234");
		System.out.println(password);
		assertTrue("The password was not long enough", password == true);
	}	
	@Test
	public void testCheckPasswordLengthException() {
		boolean password = passwordValidator.checkPasswordLength("Seena1234");
		System.out.println(password);
		assertTrue("The password was not long enough", password == true);
	}	
	@Test
	public void testCheckPasswordLengthBoundaryIN() {
		boolean password = passwordValidator.checkPasswordLength("Seena123");
		System.out.println(password);
		assertTrue("The password was not long enough", password == true);
	}	
	@Test (expected = NumberFormatException.class)
	public void testCheckPasswordLengthBoundaryOut(){
		boolean password = passwordValidator.checkPasswordLength("Seena12");
		System.out.println(password);
		assertTrue("The password was not long enough", password == false);
	}	
	@Test
	public void testCheckDigits() {
		boolean password = passwordValidator.checkDigits("Seena1234");
		System.out.println(password);
		assertTrue("The password does not contain enough digits.", password == true);
	}
	@Test(expected = NumberFormatException.class)
	public void testCheckDigitsException() {
		boolean password = passwordValidator.checkDigits("Seena");
		System.out.println(password);
		assertTrue("The password does not contain enough digits.", password == true);
	}
	@Test
	public void testCheckDigitsBoundaryIn() {
		boolean password = passwordValidator.checkDigits("Seena12");
		System.out.println(password);
		assertTrue("The password does not contain enough digits.", password == true);
	}
	@Test(expected = NumberFormatException.class)
	public void testCheckDigitsBoundaryOut() {
		boolean password = passwordValidator.checkDigits("Seena1");
		System.out.println(password);
		assertTrue("The password does not contain enough digits.", password == false);
	}


}
